#include "rosfovis/ros_visualization.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "fovis/fovis.hpp"
#include "rosfovis/algo_util.hpp"

namespace ca
{

static const float ORANGE_TO_BLUE_RGB[12][3] = {
  {1.000,   0.167,   0.000},
  {1.000,   0.400,   0.100},
  {1.000,   0.600,   0.200},
  {1.000,   0.800,   0.400},
  {1.000,   0.933,   0.600},
  {1.000,   1.000,   0.800},
  {0.800,   1.000,   1.000},
  {0.600,   0.933,   1.000},
  {0.400,   0.800,   1.000},
  {0.200,   0.600,   1.000},
  {0.100,   0.400,   1.000},
  {0.000,   0.167,   1.000},
};

// TODO deprecate? used from stereo
FovisVisualization::FovisVisualization(const fovis::StereoCalibration *calib) {
  // take the  Z corresponding to disparity 5 px as 'max Z'
  Eigen::Matrix4d uvdtoxyz = calib->getUvdToXyz();
  Eigen::Vector4d xyzw = uvdtoxyz * Eigen::Vector4d(1, 1, 5, 1);
  xyzw /= xyzw.w();
  max_z_ = xyzw.z();

  // take the  Z corresponding to 3/4 disparity img width px as 'min Z'
  xyzw = uvdtoxyz * Eigen::Vector4d(1, 1, (3*calib->getWidth())/4, 1);
  xyzw /= xyzw.w();
  min_z_ = xyzw.z();
  ROS_INFO("rosviz min z: %f, max_z: %f", min_z_, max_z_);
}

FovisVisualization::FovisVisualization() {
  ros::NodeHandle nh("~");
  params_.get_from_node_handle(nh);
  min_z_ = params_.min_z;
  max_z_ = params_.max_z;

  //ROS_INFO("rosviz min z: %f, max_z: %f", min_z_, max_z_);
}

void FovisVisualization::colormap(float z, cv::Scalar& bgr) {
  // normalize to 0,1 range
  float t = z;
  ca::update_clip(t, min_z_, max_z_-1e-6f);
  t /= (max_z_ - min_z_);
  // TODO a nonlinear mapping
  int max_range = (CA_STATIC_ARRAY_SIZE(ORANGE_TO_BLUE_RGB) - 1);
  int row = static_cast<int>(floor(max_range*t));
  //std::cerr << "z = " << z << " t = " << t << " row = " << row << " ";
  if (row >= max_range) {
    bgr[0] = static_cast<int>(ORANGE_TO_BLUE_RGB[max_range][2]*255);
    bgr[1] = static_cast<int>(ORANGE_TO_BLUE_RGB[max_range][1]*255);
    bgr[2] = static_cast<int>(ORANGE_TO_BLUE_RGB[max_range][0]*255);
    //std::cerr << "bgr: " << bgr[0] << "," << bgr[1] << "," << bgr[2] << std::endl;
  } else if (row == 0) {
    bgr[0] = static_cast<int>(ORANGE_TO_BLUE_RGB[0][2]*255);
    bgr[1] = static_cast<int>(ORANGE_TO_BLUE_RGB[0][1]*255);
    bgr[2] = static_cast<int>(ORANGE_TO_BLUE_RGB[0][0]*255);
    //std::cerr << "bgr: " << bgr[0] << "," << bgr[1] << "," << bgr[2] << std::endl;
  } else {
    float w = (max_range*t) - row;
    bgr[0] = static_cast<int>((ORANGE_TO_BLUE_RGB[row][2]*w + ORANGE_TO_BLUE_RGB[row+1][0]*(1.-w))*255);
    bgr[1] = static_cast<int>((ORANGE_TO_BLUE_RGB[row][1]*w + ORANGE_TO_BLUE_RGB[row+1][1]*(1.-w))*255);
    bgr[2] = static_cast<int>((ORANGE_TO_BLUE_RGB[row][0]*w + ORANGE_TO_BLUE_RGB[row+1][2]*(1.-w))*255);
    //std::cerr << "bgr: " << bgr[0] << "," << bgr[1] << "," << bgr[2] << "w: " << w << std::endl;
  }
}

void FovisVisualization::draw_keypoints(const fovis::VisualOdometry *odom,
                                        cv::Mat& out_buffer) {
  using namespace fovis;

  const OdometryFrame* target_frame = odom->getTargetFrame();
  cv::Scalar bgr(0, 0, 0);
  for (int i=0, num_levels=target_frame->getNumLevels();
       i < num_levels;
       ++i) {
    const PyramidLevel *level = target_frame->getLevel(i);
    for (int j=0, num_kp=level->getNumKeypoints();
         j < num_kp;
         ++j) {
      const KeypointData& kp_data(*level->getKeypointData(j));
      cv::Point2f center(kp_data.base_uv.x(), kp_data.base_uv.y());
      if (params_.viz_keypoint_2d) {
        cv::circle(out_buffer, center, 1, bgr, -1);
      }
      if (params_.viz_keypoint_patch) {
        // this draws a rectangle around the patch.
        int scaling = 4*(1<<kp_data.pyramid_level);
        cv::Point2f top_left(kp_data.base_uv.x()-4*scaling, kp_data.base_uv.y()-4*scaling);
        cv::Point2f bottom_right(kp_data.base_uv.x()+4*scaling, kp_data.base_uv.y()+4*scaling);
        cv::rectangle(out_buffer, top_left, bottom_right, bgr);
      }
    }
  }
}

void FovisVisualization::draw_flow(const fovis::VisualOdometry *odom, cv::Mat& out_buffer) {
  using namespace fovis;
  const MotionEstimator* estimator = odom->getMotionEstimator();
  const FeatureMatch* matches = estimator->getMatches();
  int num_matches = estimator->getNumMatches();

  for (int i=0; i<num_matches; ++i) {
    const FeatureMatch& match = matches[i];
    if (!match.inlier) { continue; }
    cv::Scalar bgr;
    //float disp = match.target_keypoint->disparity;
    //std::cerr << "disp = " << disp << " z = " << match.target_keypoint->xyz.z() << std::endl;
    colormap(match.target_keypoint->xyz.z(), bgr);
    float cur_x = match.target_keypoint->base_uv.x();
    float cur_y = match.target_keypoint->base_uv.y();
    cv::Point2f cur_pt(cur_x, cur_y);
    cv::circle(out_buffer, cur_pt, 3, bgr, -1, CV_AA);
    // draw ref-to-target 'flow'
    float prev_x = match.ref_keypoint->base_uv.x();
    float prev_y = match.ref_keypoint->base_uv.y();
    cv::Point2f prev_pt(prev_x, prev_y);
    cv::line(out_buffer, cur_pt, prev_pt, bgr, 1, CV_AA);
  }
}

void FovisVisualization::draw_homography(const fovis::VisualOdometry* odom, cv::Mat& out_buffer) {

  int width = out_buffer.cols;
  int height = out_buffer.rows;
  const Eigen::Matrix3d& H = odom->getInitialHomography();
  //Eigen::Matrix3d H; H.setIdentity();
  Eigen::MatrixXd vertices(4, 3);
  double aspect_ratio = static_cast<double>(width)/height;
#if 0
  vertices <<
        0     , 0      , 1,
        width , 0      , 1,
        width , height , 1,
        0     , height , 1;
#endif
  int sq_sz = 120;
  vertices <<
      width/2 - sq_sz*aspect_ratio, height/2 - sq_sz, 1,
      width/2 + sq_sz*aspect_ratio, height/2 - sq_sz, 1,
      width/2 + sq_sz*aspect_ratio, height/2 + sq_sz, 1,
      width/2 - sq_sz*aspect_ratio, height/2 + sq_sz, 1;
  Eigen::MatrixXd warped_points = H*vertices.transpose();
  warped_points.row(0) = warped_points.row(0).array()/warped_points.row(2).array();
  warped_points.row(1) = warped_points.row(1).array()/warped_points.row(2).array();
  //const cv::Point *cv_pts[4];

  std::vector<cv::Point> cv_pts;
  for (int i=0; i < 4; ++i) {
    cv_pts.push_back(cv::Point2i(vertices(i, 0), vertices(i, 1)));
  }
  const cv::Point * pt = &cv_pts[0];
  int npts = 4;
  cv::polylines(out_buffer, &pt, &npts, 1, true, cv::Scalar(0, 255, 255), 2, CV_AA);

  for (int i=0; i < 4; ++i) {
    cv_pts[i] = cv::Point2i(warped_points(0, i), warped_points(1, i));
  }
  cv::polylines(out_buffer, &pt, &npts, 1, true, cv::Scalar(255, 255, 0), 2, CV_AA);
}

void FovisVisualization::draw(const fovis::VisualOdometry *odom, cv::Mat& out_buffer) {
  using namespace fovis;
  const OdometryFrame* target_frame = odom->getTargetFrame();

  int width = target_frame->getLevel(0)->getWidth();
  int height = target_frame->getLevel(0)->getHeight();
  cv::Mat gray_buffer_ = cv::Mat(height, width, CV_8UC1,
                         const_cast<uint8_t*>(target_frame->getLevel(0)->getGrayscaleImage()),
                         target_frame->getLevel(0)->getGrayscaleImageStride());
  // convert to color so we can draw on it
  cv::cvtColor(gray_buffer_, out_buffer, CV_GRAY2BGR);

  if (params_.viz_keypoint_2d || params_.viz_keypoint_patch) {
    draw_keypoints(odom, out_buffer);
  }
  if (params_.viz_flow) {
    draw_flow(odom, out_buffer);
  }
  if (params_.viz_homo) {
    draw_homography(odom, out_buffer);
  }

}

}
