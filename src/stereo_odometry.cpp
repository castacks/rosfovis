//#define USE_LCM

#include <string>

#include <boost/scoped_ptr.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/PoseStamped.h>
#include <image_transport/image_transport.h>
#include <image_transport/subscriber_filter.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/image_encodings.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#ifdef USE_LCM
#include <bot_lcmgl_client/lcmgl.h>
#include <lcm/lcm.h>
#include <lcmtypes/bot_core.h>
#include <lcmtypes/bot_core_image_t.h>
#include "rosfovis/botlcm_visualization.hpp"
#endif

#include <geom_cast/geom_cast.hpp>
#include <tictoc_profiler/profiler.hpp>

#include "fovis/fovis.hpp"

#include "rosfovis/fovis_update.h"
#include "rosfovis/ros_visualization.hpp"
#include "rosfovis/calibration.hpp"

#include "diagnostic_updater/diagnostic_updater.h"
#include "diagnostic_updater/publisher.h"

#undef NO_DATA
namespace ca
{

struct StereoOdometryDiagnosticParams {

  fovis::MotionEstimateStatusCode est_status;

  int no_data_, success_, insufficient_inliers_, optim_fail_, reproj_error_;

  int no_inliers_, reproject_failures_, no_matches_, no_keypoints_;

  double reprojection_error_;

  StereoOdometryDiagnosticParams() :
    est_status(fovis::NO_DATA),
    no_data_(0),
    success_(0),
    insufficient_inliers_(0),
    optim_fail_(0),
    reproj_error_(0),
    no_inliers_(0),
    reproject_failures_(0),
    no_matches_(0),
    no_keypoints_(0),
    reprojection_error_(0.0) { }

  void setEstimatorParams(int a, int b, int c,int d, double e) {
    no_inliers_ = a;
    reproject_failures_ = b;
    no_matches_ = c;
    no_keypoints_ = d;
    reprojection_error_ = e;
  }
};

class StereoOdometryNode {
 public:
  StereoOdometryNode() :
      it_(nh_),
      sync_(3),
      odom_(NULL),
      depth_producer_(NULL),
#ifdef USE_LCM
      bot_visualization_(NULL),
#endif
      image_count_(0),
      got_calibration_(false),
      current_ts_(0),
      last_ts_(0),
      odom_seq("0"),
      raw_images_(true),
      resized_(false),
      bot_visualize_(true),
      ros_visualize_(true),
      enable_profiling_(true),
      camera_frequency_(0.0)
  {
    init();
  }

  virtual ~StereoOdometryNode() {
#ifdef USE_LCM
    if (publish_lcm_) { lcm_destroy(publish_lcm_); }
#endif
  }

 private:

  void image_cb(const sensor_msgs::ImageConstPtr& l_image,
                const sensor_msgs::CameraInfoConstPtr& l_cam_info,
                const sensor_msgs::ImageConstPtr& r_image,
                const sensor_msgs::CameraInfoConstPtr& r_cam_info) {
    ++image_count_;
    last_ts_ = current_ts_;
    current_ts_ = l_image->header.stamp;

    //ROS_INFO_STREAM("enc: " << l_image->encoding << " " << r_image->encoding << "\n");

    if (!got_calibration_) {
      if (raw_images_) {
        got_calibration_ = ca_rosfovis::get_calibration_raw(l_cam_info, r_cam_info, stereo_params_);
      } else {
        got_calibration_ = ca_rosfovis::get_calibration(l_cam_info, r_cam_info, resized_, stereo_params_);
      }
      this->init_vo();
    }

    uint8_t *left_data(NULL), *right_data(NULL);

    ca::Profiler::tictoc("convert_image");
    if (raw_images_) {
      // Convert ROS messages for use with OpenCV
      try {
        cv_bridge::CvImageConstPtr l_cvimage = cv_bridge::toCvShare(l_image, sensor_msgs::image_encodings::BAYER_RGGB8);
        cv_bridge::CvImageConstPtr r_cvimage = cv_bridge::toCvShare(r_image, sensor_msgs::image_encodings::BAYER_RGGB8);
        cv::cvtColor(l_cvimage->image, left_, CV_BayerBG2GRAY);
        cv::cvtColor(r_cvimage->image, right_, CV_BayerBG2GRAY);
        left_data = left_.data;
        right_data = right_.data;
      } catch (cv_bridge::Exception& e) {
        ROS_ERROR("Conversion error: %s", e.what());
        return;
      }
    } else {
      try {
        cv_bridge::CvImageConstPtr l_cvimage = cv_bridge::toCvShare(l_image, sensor_msgs::image_encodings::MONO8);
        cv_bridge::CvImageConstPtr r_cvimage = cv_bridge::toCvShare(r_image, sensor_msgs::image_encodings::MONO8);
        left_data = l_cvimage->image.data;
        right_data = r_cvimage->image.data;
      } catch (cv_bridge::Exception& e) {
        ROS_ERROR("Conversion error: %s", e.what());
        return;
      }
    }
    ca::Profiler::tictoc("convert_image");

    //cv::imshow("left", left_);
    //cv::imshow("right", right_);
    //cv::waitKey(10);

    ca::Profiler::tictoc("vo");
    depth_producer_->setRightImage(right_data);
    odom_->processFrame(left_data, depth_producer_.get());
    ca::Profiler::tictoc("vo");

    this->publish_measurements();
    // diagnostics
    if (cbFreqDiag_) {
      cbFreqDiag_->tick();
    }
    updater_.update();
  }

  void publish_measurements() {

    const fovis::MotionEstimator * me(odom_->getMotionEstimator());

    Eigen::Isometry3d cam_to_local = odom_->getPose();
    // rotate coordinate frame so that look vector is +X, and down is +Z (ned frame)
    Eigen::Matrix3d M;
    M <<  0,  0, 1,
          1,  0, 0,
          0,  1, 0;
    cam_to_local = M * cam_to_local;
    Eigen::Vector3d translation(cam_to_local.translation());
    Eigen::Quaterniond rotation(cam_to_local.rotation());
    rotation = rotation * M.transpose();

    // accumulated pose message.
    geometry_msgs::PoseStamped pose_msg;
    pose_msg.header.stamp = current_ts_;
    pose_msg.header.frame_id = "/world";
    pose_msg.pose.position = ca::point_cast<geometry_msgs::Point>(translation);
    pose_msg.pose.orientation = ca::rot_cast<geometry_msgs::Quaternion>(rotation);

    // accumulated odometry message, same info as above.
    nav_msgs::Odometry accum_odom_msg;
    accum_odom_msg.header.stamp = current_ts_;
    accum_odom_msg.header.frame_id = "/world";
    accum_odom_msg.child_frame_id = "/fovis";
    accum_odom_msg.pose.pose.position = ca::point_cast<geometry_msgs::Point>(translation);
    accum_odom_msg.pose.pose.orientation = ca::rot_cast<geometry_msgs::Quaternion>(rotation);

    // relative pose message.
    // Take the inverse --- just for navframe setup (nothing to do with fovis)
    // navframe takes odom input from framid(curr_frame) to
    // childframid(prev_frame)
    Eigen::Isometry3d motion_estimate = (odom_->getMotionEstimate()).inverse();
    Eigen::Vector3d motion_T = motion_estimate.translation();
    Eigen::Quaterniond motion_R = Eigen::Quaterniond(motion_estimate.rotation());
    const Eigen::MatrixXd & motion_cov(odom_->getMotionEstimateCov());
    Eigen::MatrixXd pose_cov = .01*Eigen::Matrix<double, 6, 6>::Identity();
    nav_msgs::Odometry odom_msg;
    odom_msg.header.stamp = current_ts_;
    odom_msg.header.frame_id = "/cur_frame"; // TODO what to name this?
    odom_msg.child_frame_id = "/prev_frame";
    odom_msg.pose.pose.position = ca::point_cast<geometry_msgs::Point>(motion_T);
    odom_msg.pose.pose.orientation = ca::rot_cast<geometry_msgs::Quaternion>(motion_R);

    double dt = (current_ts_ - last_ts_).toSec();
    if (dt > 0) {
      Eigen::Vector3d linear(motion_T/dt);
      odom_msg.twist.twist.linear = ca::point_cast<geometry_msgs::Vector3>(linear);
      tf::Quaternion tfq = ca::rot_cast<tf::Quaternion>(motion_R);
      double angle = tfq.getAngle();
      tf::Vector3 axis = tfq.getAxis();
      tf::Vector3 twist = axis * (angle/dt);
      odom_msg.twist.twist.angular = ca::point_cast<geometry_msgs::Vector3>(twist);
      for (int i=0;i<6;++i) {
        for (int j=0;j<6;++j) {
          odom_msg.twist.covariance[j*6+i] = motion_cov(i, j);
        }
      }
    }

    // data about how fovis is doing
    rosfovis::fovis_update update_msg;
    update_msg.header.stamp = current_ts_;

    fovis::MotionEstimateStatusCode estim_status = odom_->getMotionEstimateStatus();
    diag_.est_status = estim_status;
    switch(estim_status) {
      case fovis::NO_DATA:
        update_msg.estimate_status = rosfovis::fovis_update::NO_DATA;
        diag_.no_data_++;
        break;
      case fovis::SUCCESS:
        update_msg.estimate_status = rosfovis::fovis_update::ESTIMATE_VALID;
        ROS_INFO("Inliers: %4d  Rep. fail: %4d Matches: %4d Feats: %4d Mean err: %5.2f",
          me->getNumInliers(),
          me->getNumReprojectionFailures(),
          me->getNumMatches(),
          odom_->getTargetFrame()->getNumKeypoints(),
          me->getMeanInlierReprojectionError());
          for (int i=0;i<6;++i) {
            for (int j=0;j<6;++j) {
              odom_msg.pose.covariance[j*6+i] = (me->getNumMatches()/me->getNumInliers())*pose_cov(i,j);
            }
          }
          diag_.success_++;
        break;
      case fovis::INSUFFICIENT_INLIERS:
        update_msg.estimate_status = rosfovis::fovis_update::ESTIMATE_INSUFFICIENT_FEATURES;
        ROS_INFO("Insufficient inliers");
        diag_.insufficient_inliers_++;
        break;
      case fovis::OPTIMIZATION_FAILURE:
        update_msg.estimate_status = rosfovis::fovis_update::ESTIMATE_DEGENERATE;
        ROS_INFO("Unable to solve for rigid body transform");
        diag_.optim_fail_++;
        break;
      case fovis::REPROJECTION_ERROR:
        update_msg.estimate_status = rosfovis::fovis_update::ESTIMATE_REPROJECTION_ERROR;
        ROS_INFO("Excessive reprojection error (%f).", me->getMeanInlierReprojectionError());
        diag_.reproj_error_++;
        break;
      default:
        ROS_INFO("Unknown error (this should never happen)");
        ROS_BREAK();
        break;
    }

    diag_.setEstimatorParams(me->getNumInliers(),
                             me->getNumReprojectionFailures(),
                             me->getNumMatches(),
                             odom_->getTargetFrame()->getNumKeypoints(),
                             me->getMeanInlierReprojectionError());
    //std::cout<<std::endl;

    update_msg.change_reference_frame = odom_->getChangeReferenceFrames();
    update_msg.num_matches = me->getNumMatches();
    update_msg.num_inliers = me->getNumInliers();
    update_msg.mean_reprojection_error = me->getMeanInlierReprojectionError();
    update_msg.num_reprojection_failures = me->getNumReprojectionFailures();
    const fovis::OdometryFrame * tf(odom_->getTargetFrame());
    update_msg.num_detected_keypoints = tf->getNumDetectedKeypoints();
    update_msg.num_keypoints = tf->getNumKeypoints();
    update_msg.fast_threshold = odom_->getFastThreshold();

    // TODO should we only publish if success?
    if (estim_status == fovis::SUCCESS) {
      pose_pub_.publish(pose_msg);
      accum_odom_pub_.publish(accum_odom_msg);
      rel_odom_pub_.publish(odom_msg);
#ifdef USE_LCM
      if (bot_visualize_) { bot_visualization_->draw(odom_.get()); }
#endif
    }

    fovis_update_pub_.publish(update_msg);

    if (ros_visualize_ && image_vis_pub_.getNumSubscribers() > 0) {
      ca::Profiler::tictoc("ros_viz");
      ros_visualization_->draw(odom_.get(), out_buffer_);
      ca::Profiler::tictoc("ros_viz");
      cv_bridge::CvImage img_msg;
      img_msg.header.stamp = current_ts_;
      img_msg.header.frame_id = "world"; // TODO get frame id from input
      img_msg.encoding = sensor_msgs::image_encodings::BGR8;
      img_msg.image = out_buffer_;
      image_vis_pub_.publish(img_msg.toImageMsg());
    }
  }

  void init() {
    // get params, settings
    // TODO the idiomatic thing?
    if (!nh_.getParam("raw_images", raw_images_)) {
      ROS_WARN("raw_images parameter not found, assuming true");
    }
    if (!nh_.getParam("resized", resized_)) {
      ROS_WARN("resized parameter not found, assuming false");
    }

    if (!nh_.getParam("bot_visualize", bot_visualize_)) { /* ctor's default */}
    if (!nh_.getParam("ros_visualize", ros_visualize_)) { /* ctor's default */}

    if (!nh_.getParam("enable_profiling", enable_profiling_)) { /* ctor's default */ }
    if (!nh_.getParam("camera_frequency",camera_frequency_)) { /* ctor's default */ }
    if (enable_profiling_) {
      ROS_INFO("Profiling enabled");
      ca::Profiler::enable();
    }

    // subscribe
    l_info_sub_.subscribe(nh_, "left/camera_info", 1);
    r_info_sub_.subscribe(nh_, "right/camera_info", 1);

    // TODO correctly handle namespaces, etc
    if (raw_images_) {
      ROS_INFO("Subscribing to image_raw");
      l_image_sub_.subscribe(it_, "left/image_raw", 1);
      r_image_sub_.subscribe(it_, "right/image_raw", 1);
    } else {
      if (resized_) {
        ROS_INFO("Subscribing to image_rect_resized. Assuming half resolution.");
        l_image_sub_.subscribe(it_, "left/image_rect", 1);
        r_image_sub_.subscribe(it_, "right/image_rect", 1);
      } else {
        ROS_INFO("Subscribing to image_rect.");
        l_image_sub_.subscribe(it_, "left/image_rect", 1);
        r_image_sub_.subscribe(it_, "right/image_rect", 1);
      }
    }

    sync_.connectInput(l_image_sub_, l_info_sub_, r_image_sub_, r_info_sub_);
    sync_.registerCallback(boost::bind(&StereoOdometryNode::image_cb, this, _1, _2, _3, _4));

    // Dynamic reconfigure for parameters
    //ReconfigureServer::CallbackType f = boost::bind(&VONode::configCb, this, _1, _2);
    //reconfigure_server_.setCallback(f);

    // advertise publications
    pose_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("pose", 1);
    accum_odom_pub_ = nh_.advertise<nav_msgs::Odometry>("/visualodometry/pose", 1);
    rel_odom_pub_ = nh_.advertise<nav_msgs::Odometry>("/visualodometry/measurement", 1);
    fovis_update_pub_ = nh_.advertise<rosfovis::fovis_update>("/visualodometry/fovis_update", 1);
    if (ros_visualize_) {
      ROS_INFO("ROS visualization enabled.");
      image_vis_pub_ = it_.advertise("vo_vis", 1);
    }

    //Diagnostics
    updater_.setHardwareID("fovis");
    updater_.add("Statistics", this, &StereoOdometryNode::produce_diagnostics);
    if(camera_frequency_){
      cbFreqDiag_.reset(new diagnostic_updater::HeaderlessTopicDiagnostic("Callback Frequency",
                     updater_,
                     diagnostic_updater::FrequencyStatusParam(&camera_frequency_, &camera_frequency_, 0.1, 10)));
    }
  }

  void produce_diagnostics(diagnostic_updater::DiagnosticStatusWrapper &stat){
    switch(diag_.est_status){
      case fovis::NO_DATA:
        stat.summary(diagnostic_msgs::DiagnosticStatus::WARN, "No Data.");
        break;
      case fovis::SUCCESS:
        stat.summary(diagnostic_msgs::DiagnosticStatus::OK, "Success.");
        break;
      case fovis::INSUFFICIENT_INLIERS:
        stat.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "Insufficient Inliers.");
        break;
      case fovis::OPTIMIZATION_FAILURE:
        stat.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "Optimization Failure.");
        break;
      case fovis::REPROJECTION_ERROR:
        stat.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "Reprojection Error.");
        break;
    }
    stat.add("Total Features",diag_.no_keypoints_);
    stat.add("No of Matches",diag_.no_matches_);
    stat.add("Reprojection Failures",diag_.reproject_failures_);
    stat.add("Reprojection Error",diag_.reproj_error_);
    stat.add("Final Inliers",diag_.no_inliers_);
    stat.add("Total Successes",diag_.success_);
    stat.add("Total Failures",diag_.no_data_+diag_.insufficient_inliers_+diag_.optim_fail_+diag_.reproj_error_);
  }

  void init_vo() {
    // this has to be delayed until we get calibration from camerainfo
    ROS_ASSERT(got_calibration_);

    // Note VisualOdometry is borrowing the rectification pointer.
    // It's never destroyed but it's not a problem since there is a single instance.
    fovis::StereoCalibration *calib = new fovis::StereoCalibration(stereo_params_);
    fovis::VisualOdometryOptions vo_opts(get_parameters());
    odom_.reset(new fovis::VisualOdometry(calib->getLeftRectification(), vo_opts));
    depth_producer_.reset(new fovis::StereoDepth(calib, vo_opts));

#ifdef USE_LCM
    if (bot_visualize_) {
      ROS_INFO("bot LCM visualization enabled.");
      publish_lcm_ = lcm_create(NULL);
      bot_lcmgl_t* lcmgl = bot_lcmgl_init(publish_lcm_, "stereo-odometry");
      bot_visualization_.reset(new fovis::Visualization(lcmgl, calib));
    }
#endif
    if (ros_visualize_) { ros_visualization_.reset(new FovisVisualization(calib)); }
  }

  fovis::VisualOdometryOptions get_parameters() {
    fovis::VisualOdometryOptions vo_opts = fovis::VisualOdometry::getDefaultOptions();

    // note that we are actually using slightly different default options, better
    // (AFAICT) for stereo.

    nh_.param<std::string>("feature_window_size", vo_opts["feature-window-size"], "9");
    nh_.param<std::string>("max_pyramid_level", vo_opts["max-pyramid-level"], "3");
    nh_.param<std::string>("min_pyramid_level", vo_opts["min-pyramid-level"], "0");
    nh_.param<std::string>("target_pixels_per_feature", vo_opts["target-pixels-per-feature"], "100");
    nh_.param<std::string>("fast_threshold", vo_opts["fast-threshold"], "9");
    nh_.param<std::string>("fast_threshold_adaptive_gain", vo_opts["fast-threshold-adaptive-gain"], "0.002");
    nh_.param<std::string>("use_adaptive_threshold", vo_opts["use-adaptive-threshold"], "true");
    nh_.param<std::string>("use_homography_initialization", vo_opts["use-homography-initialization"], "true");
    nh_.param<std::string>("ref_frame_change_threshold", vo_opts["ref-frame-change-threshold"], "200");
    nh_.param<std::string>("initial_rotation_pyramid_level", vo_opts["initial-rotation-pyramid-level"], "3");

    // OdometryFrame
    nh_.param<std::string>("use_bucketing", vo_opts["use-bucketing"], "true");
    nh_.param<std::string>("bucket_width", vo_opts["bucket-width"], "50");
    nh_.param<std::string>("bucket_height", vo_opts["bucket-height"], "50");
    nh_.param<std::string>("max_keypoints_per_bucket", vo_opts["max-keypoints-per-bucket"], "25");
    nh_.param<std::string>("use_image_normalization", vo_opts["use-image-normalization"], "false");

    // MotionEstimator
    nh_.param<std::string>("inlier_max_reprojection_error", vo_opts["inlier-max-reprojection-error"], "1.5");
    nh_.param<std::string>("clique_inlier_threshold", vo_opts["clique-inlier-threshold"], "0.2");
    nh_.param<std::string>("min_features_for_estimate", vo_opts["min-features-for-estimate"], "10");
    nh_.param<std::string>("max_mean_reprojection_error", vo_opts["max-mean-reprojection-error"], "10.0");
    nh_.param<std::string>("use_subpixel_refinement", vo_opts["use-subpixel-refinement"], "true");
    nh_.param<std::string>("feature_search_window", vo_opts["feature-search-window"], "25");
    nh_.param<std::string>("update_target_features_with_refined", vo_opts["update-target-features-with-refined"], "false");

    // StereoDepth
    nh_.param<std::string>("stereo_require_mutual_match", vo_opts["stereo-require-mutual-match"], "true");
    nh_.param<std::string>("stereo_max_dist_epipolar_line", vo_opts["stereo-max-dist-epipolar-line"], "2.0");
    nh_.param<std::string>("stereo_max_refinement_displacement", vo_opts["stereo-max-refinement-displacement"], "2.0");
    nh_.param<std::string>("stereo_max_disparity", vo_opts["stereo-max-disparity"], "128");

    return vo_opts;
  }

 private:
  // data
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;

  // Subscriptions
  image_transport::SubscriberFilter l_image_sub_, r_image_sub_;
  message_filters::Subscriber<sensor_msgs::CameraInfo> l_info_sub_, r_info_sub_;

  // Publishers
  ros::Publisher pose_pub_;
  ros::Publisher accum_odom_pub_;
  ros::Publisher rel_odom_pub_;
  ros::Publisher fovis_update_pub_;
  image_transport::Publisher image_vis_pub_;

  message_filters::TimeSynchronizer<sensor_msgs::Image, sensor_msgs::CameraInfo,
      sensor_msgs::Image, sensor_msgs::CameraInfo> sync_;

  //Diagnostics
  diagnostic_updater::Updater updater_;
  StereoOdometryDiagnosticParams diag_;
  boost::shared_ptr<diagnostic_updater::HeaderlessTopicDiagnostic> cbFreqDiag_;

  boost::scoped_ptr<fovis::VisualOdometry> odom_;
  boost::scoped_ptr<fovis::StereoDepth> depth_producer_;
  fovis::StereoCalibrationParameters stereo_params_;

#ifdef USE_LCM
  lcm_t * publish_lcm_;
  boost::scoped_ptr<fovis::Visualization> bot_visualization_;
#endif
  boost::scoped_ptr<FovisVisualization> ros_visualization_;

  cv::Mat left_, right_;
  cv::Mat out_buffer_;

  uint64_t image_count_;
  bool got_calibration_;

  ros::Time current_ts_, last_ts_;
  std::stringstream odom_seq;

  // misc parameters
  bool raw_images_;
  bool resized_;
  bool bot_visualize_, ros_visualize_;
  bool enable_profiling_;
  double camera_frequency_;
};

}

int main(int argc, char** argv) {

  ros::init(argc, argv, "stereo_odometry");

  ca::StereoOdometryNode so_node;
  ros::spin();

  ROS_INFO("done");
  ca::Profiler::print_aggregated(std::cerr);
  return 0;
}
