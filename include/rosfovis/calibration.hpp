#ifndef CALIBRATION_HPP_OE0AJSJZ
#define CALIBRATION_HPP_OE0AJSJZ

#include <sensor_msgs/CameraInfo.h>
#include <fovis/stereo_calibration.hpp>

namespace ca_rosfovis {

/**
 * For the raw, unresized, distorted, unrectified images.
 * TODO: maybe we can use image_geometry
 */
bool get_calibration_raw(sensor_msgs::CameraInfoConstPtr l_cam_info,
                         sensor_msgs::CameraInfoConstPtr r_cam_info,
                         fovis::StereoCalibrationParameters& stereo_params);

/**
 * This is for the rectified+undistorted and possibly downsampled images.
 * TODO: maybe we can use image_geometry
 */
bool get_calibration(sensor_msgs::CameraInfoConstPtr l_cam_info,
                     sensor_msgs::CameraInfoConstPtr r_cam_info,
                     bool resized,
                     fovis::StereoCalibrationParameters& stereo_params);

}
#endif /* end of include guard: CALIBRATION_HPP_OE0AJSJZ */
